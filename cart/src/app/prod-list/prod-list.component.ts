import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prod-list',
  templateUrl: './prod-list.component.html',
  styleUrls: ['./prod-list.component.css']
})
export class ProdListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  item: Object[] = [
    {"name":"Item 1","price":100,"img":"assets/img/1.jpg"},
    {"name":"Item 2","price":200,"img":"assets/img/2.jpg"},
    {"name":"Item 3","price":300,"img":"assets/img/3.jpg"}
  ];
  cart: Object[] = [
    {"name":"Item 1","quantity":1}
  ];
  newItem="";
  addToCart(name){
    for (var i = (this.cart.length-1); i >= 0 ; i--) {
      var obj = this.cart[i];
      if (obj["name"]==name)
      {
        obj["quantity"]++;
      }
      else if(i==0)
      {
        this.cart.push({"name":name,"quantity":1});
      }
    }
  }
}
