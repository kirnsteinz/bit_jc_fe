import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  todoList: Object[] = [
    {"name":"Task 1","category":"A","status":0},
    {"name":"Task 2","category":"B","status":0},
    {"name":"Task 3","category":"C","status":0}
  ];
  newtaskname ="";
  newtaskcategory ="";
  addTodo(){
    this.todoList.push({"name":this.newtaskname,"category":this.newtaskcategory,"status":0})
  }
  delete(index){
    this.todoList.splice(index, 1);
  }

  updateTask(){
    for (var i = (this.todoList.length-1); i >= 0 ; i--) {
      var obj = this.todoList[i];
      if (obj["status"]==1)
        {this.todoList.splice(i,1);}
    }
  }

}
